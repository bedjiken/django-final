from django.db import models
from django.contrib.auth.models import User
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, SmartResize

# Create your models here.

class Personnality(models.Model):

	id_personality = models.AutoField(primary_key = True)
	first_name = models.CharField(max_length = 50)
	last_name = models.CharField(max_length = 50)
	birthday = models.DateField(null=True, blank=True)
	diplome_de = models.CharField(max_length = 50)
	profession = models.CharField(max_length = 50)
	birth_place = models.CharField(max_length = 50)

	MASCULIN = 'M'
	FEMININ = 'F'

	SEX_CHOICE = (
	(MASCULIN, 'M'),
	(FEMININ, 'F'),
	)

	sex = models.CharField(max_length=20,
	choices=SEX_CHOICE,
	default=MASCULIN)

	POLITIQUE = 'Politique'
	EDUCATION = 'Education'
	MUSIQUE = 'Musique'
	CINEMA = 'Cinema'
	ENTREPRENEURIAT = 'Entrepreneuriat'
	ECONOMIE = 'Economie'
	SPORT = 'Sport'
	ARTS = 'Arts'


	PROFESSION_TYPE = (
	(POLITIQUE, 'Politique'),
	(EDUCATION,  'Education'),
	(MUSIQUE, 'Musique'),
	(CINEMA , 'Cinema'),
	(ENTREPRENEURIAT, 'Entrepreneuriat'),
	(ECONOMIE, 'Economie'),
	(SPORT, 'Sport'),
	(ARTS, 'Arts'),)

	personality_type = models.CharField(max_length=20,
	choices=PROFESSION_TYPE,
	default=POLITIQUE)

	religion = models.CharField(max_length = 50)
	hometown = models.CharField(max_length = 100)
	personality_parents = models.CharField(max_length = 255)
	personality_children = models.TextField(max_length = 1000)
	nationality = models.CharField(max_length = 50)
	image_personality = models.ImageField(upload_to='image_personality')
	img_pers_thumbnail = ImageSpecField(source='image_personality',
                                      processors=[ResizeToFill(150, 100)],
                                      format='JPEG',
                                      options={'quality': 100})
	personality_spouse_name = models.CharField(max_length = 70)
	created_at = models.DateTimeField(auto_now_add = True, auto_now = False)

	def __str__(self):
		return self.first_name + " " + self.last_name



class DescriptionPersonality(models.Model):

	id_description = models.AutoField(primary_key = True)
	id_personality = models.ForeignKey(Personnality)
	description = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True, auto_now = False)

class SystemUser(models.Model):

	id_user = models.AutoField(primary_key = True)
	user = models.ForeignKey(User)
	first_name = models.CharField(max_length = 70)
	last_name = models.CharField(max_length = 70)
	email = models.EmailField(max_length = 70, unique=True)
	city = models.CharField(max_length = 50)
	user_registered_date = models.DateTimeField(auto_now_add=True, auto_now = False)

	MASCULIN = 'M'
	FEMININ = 'F'

	SEX_CHOICE = (
	(MASCULIN, 'M'),
	(FEMININ, 'F'),
	)

	sex = models.CharField(max_length=20,
	choices=SEX_CHOICE,
	default=MASCULIN)

	image_user = models.ImageField(upload_to='image_user')
	img_user_thumbnail = ImageSpecField(source='image_user',
                                      processors=[ResizeToFill(150, 100)],
                                      format='JPEG',
                                      options={'quality': 100})

class LikePersonality(models.Model):

	id_like = models.AutoField(primary_key = True)
	id_user = models.ForeignKey(SystemUser)
	id_personality = models.ForeignKey(Personnality)
	created_at = models.DateTimeField(auto_now_add=True, auto_now = False)

class MeetPersonality(models.Model):

	id_meet = models.AutoField(primary_key = True)
	id_user = models.ForeignKey(SystemUser)
	id_personality = models.ForeignKey(Personnality)
	# meeting_date = models.DateTimeField(auto_now=True, auto_now_add = False)
	created_at = models.DateTimeField(auto_now_add=True, auto_now = False)


class PersonnalityInterview(models.Model):

	id_user = models.AutoField(primary_key = True)
	id_personality = models.ForeignKey(Personnality)
	# type_interview = models.IntegerField(default = 0)
	title = models.CharField(max_length = 100)
	description = models.TextField()

	TEXT = 'Text'
	AUDIO = 'Audio'
	VIDEO = 'Video'

	TYPE_NEWS = (
	(TEXT, 'Text'),
	(AUDIO,'Audio'),
	(VIDEO, 'Video'),
	)

	type_news_media = models.CharField(max_length=20,
	choices=TYPE_NEWS,
	default=TEXT)

	POLITIQUE = 'Politique'
	EDUCATION = 'Education'
	MUSIQUE = 'Musique'
	CINEMA = 'Cinema'
	ENTREPRENEURIAT = 'Entrepreneuriat'
	ECONOMIE = 'Economie'
	SPORT = 'Sport'
	ARTS = 'Arts'


	PROFESSION_TYPE = (
	(POLITIQUE, 'Politique'),
	(EDUCATION,  'Education'),
	(MUSIQUE, 'Musique'),
	(CINEMA , 'Cinema'),
	(ENTREPRENEURIAT, 'Entrepreneuriat'),
	(ECONOMIE, 'Economie'),
	(SPORT, 'Sport'),
	(ARTS, 'Arts'),)

	interview_type = models.CharField(max_length=20,
	choices=PROFESSION_TYPE,
	default=POLITIQUE)


	created_at = models.DateTimeField(auto_now_add=True, auto_now = False)
	image_interview = models.ImageField(upload_to='image_interview')
	img_new_thumbnail = ImageSpecField(source='image_interview',
                                      processors=[ResizeToFill(150, 100)],
                                      format='JPEG',
                                      options={'quality': 100})

class News(models.Model):

	id_news = models.AutoField(primary_key = True)
	title = models.CharField(max_length = 100)

	TEXT = 'Text'
	AUDIO = 'Audio'
	VIDEO = 'Video'

	TYPE_NEWS = (
	(TEXT, 'Text'),
	(AUDIO,'Audio'),
	(VIDEO, 'Video')
	)

	type_news_media = models.CharField(max_length=20,
	choices=TYPE_NEWS,
	default=TEXT)


	POLITIQUE = 'Politique'
	EDUCATION = 'Education'
	MUSIQUE = 'Musique'
	CINEMA = 'Cinema'
	ENTREPRENEURIAT = 'Entrepreneuriat'
	ECONOMIE = 'Economie'
	SPORT = 'Sport'
	ARTS = 'Arts'


	PROFESSION_TYPE = (
	(POLITIQUE, 'Politique'),
	(EDUCATION,  'Education'),
	(MUSIQUE, 'Musique'),
	(CINEMA , 'Cinema'),
	(ENTREPRENEURIAT, 'Entrepreneuriat'),
	(ECONOMIE, 'Economie'),
	(SPORT, 'Sport'),
	(ARTS, 'Arts'),)

	news_type = models.CharField(max_length=20,
	choices=PROFESSION_TYPE,
	default=POLITIQUE)


	description = models.TextField()
	created_at = models.DateTimeField(auto_now_add=True, auto_now = False)
	image_new = models.ImageField(upload_to='image_new')
	img_new_thumbnail = ImageSpecField(source='image_new',
                                      processors=[ResizeToFill(350, 200)],
                                      format='JPEG',
                                      options={'quality': 100})
	#image = models.FileField(upload_to = 'profile/%Y/%m/%d')
	#news_image = models.FileField(upload_to="news_image/%Y/%m/%d", null=True, blank=True)
	title = models.CharField(max_length = 100)
	publisher = models.CharField(max_length = 100)

class Advertise(models.Model):

	id_adv = models.AutoField(primary_key = True)
	adv_name = models.CharField(max_length = 100)
	image_adv = models.ImageField(upload_to='image_adv')
	img_adv_thumbnail = ImageSpecField(source='image_adv',
                                      processors=[ResizeToFill(150, 100)],
                                      format='JPEG',
                                      options={'quality': 100})
	adv_created_date = models.DateTimeField(auto_now_add=True, auto_now = False)
	adv_exp_date = models.DateTimeField(auto_now = True, auto_now_add = False)
	adv_web_address = models.URLField(max_length = 150, null = False, blank = False)
	adv_description = models.TextField(max_length = 2000)
