# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Advertise',
            fields=[
                ('id_adv', models.AutoField(serialize=False, primary_key=True)),
                ('adv_name', models.CharField(max_length=100)),
                ('image_adv', models.ImageField(upload_to=b'image_adv')),
                ('adv_created_date', models.DateTimeField(auto_now_add=True)),
                ('adv_exp_date', models.DateTimeField(auto_now=True)),
                ('adv_web_address', models.URLField(max_length=150)),
                ('adv_description', models.TextField(max_length=2000)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DescriptionPersonality',
            fields=[
                ('id_description', models.AutoField(serialize=False, primary_key=True)),
                ('description', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LikePersonality',
            fields=[
                ('id_like', models.AutoField(serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MeetPersonality',
            fields=[
                ('id_meet', models.AutoField(serialize=False, primary_key=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='News',
            fields=[
                ('id_news', models.AutoField(serialize=False, primary_key=True)),
                ('type_news_media', models.CharField(default=b'Text', max_length=20, choices=[(b'Text', b'Text'), (b'Audio', b'Audio'), (b'Video', b'Video')])),
                ('news_type', models.CharField(default=b'Politique', max_length=20, choices=[(b'Politique', b'Politique'), (b'Education', b'Education'), (b'Musique', b'Musique'), (b'Cinema', b'Cinema'), (b'Entrepreneuriat', b'Entrepreneuriat'), (b'Economie', b'Economie'), (b'Sport', b'Sport'), (b'Arts', b'Arts')])),
                ('description', models.TextField()),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('image_new', models.ImageField(upload_to=b'image_new')),
                ('title', models.CharField(max_length=100)),
                ('publisher', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Personnality',
            fields=[
                ('id_personality', models.AutoField(serialize=False, primary_key=True)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=50)),
                ('birthday', models.DateField(null=True, blank=True)),
                ('diplome_de', models.CharField(max_length=50)),
                ('profession', models.CharField(max_length=50)),
                ('birth_place', models.CharField(max_length=50)),
                ('sex', models.CharField(default=b'M', max_length=20, choices=[(b'M', b'M'), (b'F', b'F')])),
                ('personality_type', models.CharField(default=b'Politique', max_length=20, choices=[(b'Politique', b'Politique'), (b'Education', b'Education'), (b'Musique', b'Musique'), (b'Cinema', b'Cinema'), (b'Entrepreneuriat', b'Entrepreneuriat'), (b'Economie', b'Economie'), (b'Sport', b'Sport'), (b'Arts', b'Arts')])),
                ('religion', models.CharField(max_length=50)),
                ('hometown', models.CharField(max_length=100)),
                ('personality_parents', models.CharField(max_length=255)),
                ('personality_children', models.TextField(max_length=1000)),
                ('nationality', models.CharField(max_length=50)),
                ('image_personality', models.ImageField(upload_to=b'image_personality')),
                ('personality_spouse_name', models.CharField(max_length=70)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PersonnalityInterview',
            fields=[
                ('id_user', models.AutoField(serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=100)),
                ('description', models.TextField()),
                ('type_news_media', models.CharField(default=b'Text', max_length=20, choices=[(b'Text', b'Text'), (b'Audio', b'Audio'), (b'Video', b'Video')])),
                ('interview_type', models.CharField(default=b'Politique', max_length=20, choices=[(b'Politique', b'Politique'), (b'Education', b'Education'), (b'Musique', b'Musique'), (b'Cinema', b'Cinema'), (b'Entrepreneuriat', b'Entrepreneuriat'), (b'Economie', b'Economie'), (b'Sport', b'Sport'), (b'Arts', b'Arts')])),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('image_interview', models.ImageField(upload_to=b'image_interview')),
                ('id_personality', models.ForeignKey(to='appuno.Personnality')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SystemUser',
            fields=[
                ('id_user', models.AutoField(serialize=False, primary_key=True)),
                ('first_name', models.CharField(max_length=70)),
                ('last_name', models.CharField(max_length=70)),
                ('email', models.EmailField(unique=True, max_length=70)),
                ('city', models.CharField(max_length=50)),
                ('user_registered_date', models.DateTimeField(auto_now_add=True)),
                ('sex', models.CharField(default=b'M', max_length=20, choices=[(b'M', b'M'), (b'F', b'F')])),
                ('image_user', models.ImageField(upload_to=b'image_user')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='meetpersonality',
            name='id_personality',
            field=models.ForeignKey(to='appuno.Personnality'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='meetpersonality',
            name='id_user',
            field=models.ForeignKey(to='appuno.SystemUser'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='likepersonality',
            name='id_personality',
            field=models.ForeignKey(to='appuno.Personnality'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='likepersonality',
            name='id_user',
            field=models.ForeignKey(to='appuno.SystemUser'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='descriptionpersonality',
            name='id_personality',
            field=models.ForeignKey(to='appuno.Personnality'),
            preserve_default=True,
        ),
    ]
