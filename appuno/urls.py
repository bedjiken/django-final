from django.conf.urls import patterns, url
from appuno import views

urlpatterns = patterns('',

    url(r'^home', views.home, name = 'home'),
    url(r'^politique-page/', views.politique_page, name = 'politique-page'),
    url(r'^news/get/(?P<id_new>\d+)/$', views.single_news, name = 'single-new'),
    url(r'^personality/get/(?P<id_personality>\d+)/$', views.personality_single, name = 'personality-single'),
    url(r'^all-news', views.all_news, name = 'all-news'),
    url(r'^all-pesonalities', views.all_pesonalities, name = 'all-pesonalities'),
    url(r'^education-page', views.education_page, name = 'education-page'),
    url(r'^events/', views.events, name = 'events'),
    #url(r'^login/', views.user_login, name = 'login'),
    #url(r'^register/', views.user_register, name = 'register'),
    #url(r'^politique/', views.politique_section, name = 'politique'),
    #url(r'^personality-home', views.personality_home, name = 'personality_home'),
    #url(r'^personality_des', views.personality_description, name = 'personality_description'),
)
