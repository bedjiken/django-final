from django.shortcuts import render, render_to_response
from appuno.models import News, Personnality
# Create your views here.

# HOME PAGE
def home(request):
    top_news = News.objects.order_by('-created_at')[: 3]
    all_news = News.objects.order_by('-created_at')
    personality_list = Personnality.objects.all().order_by('-created_at')[: 6]
    all_personalities =  Personnality.objects.all().order_by('-created_at')

    context_dict = {'top_news': top_news, 'all_news': all_news, 'personality_list': personality_list, 'all_personalities': all_personalities}
    return render(request, 'appuno/home.html', context_dict)

# POLITIQUE PAGE
def politique_page(request):
    #request the News table to get the 3 lastet news
    top_news = News.objects.filter(news_type = News.POLITIQUE).order_by('-created_at')[: 3]
    all_news = News.objects.filter(news_type = News.POLITIQUE).order_by('-created_at')
    #request the Personality table to get the a list of personality
    #personality_list = Personnality.objects.values('first_name', 'last_name', 'image_personality')
    personality_list = Personnality.objects.filter(personality_type = Personnality.POLITIQUE).order_by('-created_at')[: 6]
    all_personality = Personnality.objects.filter(personality_type = Personnality.POLITIQUE).order_by('-created_at')
    context_dict = {'news': top_news, 'personality': personality_list, 'all_news': all_news, 'all_personality': all_personality}

    #return render(request, 'appuno/home.html',{})
    return render(request, 'appuno/personality_page.html', context_dict)

#ALL NEWS PAGE
def all_news(request):
        return render(request, 'appuno/all_news.html', {'news' : News.objects.all()})

#SINGLE NEWS PAGE
def single_news(request, id_new):
    return render_to_response('appuno/single_new.html', {'new': News.objects.get(id_news = id_new)})

#SINGLE PERSONALITY PAGE
def personality_single(request, id_personality):
    return render_to_response('appuno/single_personality.html', {'personality': Personnality.objects.get(id_personality = id_personality)})

#ALL PERSONALITIES
def all_pesonalities(request):
    return render(request, 'appuno/all_personality.html', {'personalities' : Personnality.objects.all()})

# EDUCATION PAGE
def education_page(request):

    top_edu_news = all_news = News.objects.filter(news_type = News.EDUCATION).order_by('-created_at')[: 3]
    all_edu_news = News.objects.filter(news_type = News.EDUCATION)
    context_dict = {'edu_news': top_edu_news, 'all_edu_news': all_edu_news}

    return render(request, 'appuno/test.html', context_dict)


# EVENTS PAGE 
def events(request):
    return render_to_response(request, 'appuno/events.html', {})
