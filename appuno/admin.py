from django.contrib import admin
from .models import *

# Register your models here.

admin.site.register(Personnality)
admin.site.register(DescriptionPersonality)
admin.site.register(SystemUser)
admin.site.register(LikePersonality)
admin.site.register(MeetPersonality)
admin.site.register(PersonnalityInterview)
admin.site.register(News)
admin.site.register(Advertise)
