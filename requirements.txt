Django==1.7.5
django-appconf==1.0.1
django-imagekit==3.2.6
pilkit==1.1.12
six==1.9.0
