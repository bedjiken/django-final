from django.contrib import admin
from django.conf import settings
from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
#import appuno.views
from django.conf.urls.static import static
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'last.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'', include('appuno.urls')),
    url(r'^admin/', include(admin.site.urls)),
    #url(r'^admin/doc', include('django.contrib.admindocs.urls')),
) + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)

urlpatterns += staticfiles_urlpatterns()
